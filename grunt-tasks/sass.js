/* Grunt Strip Code Task Config */

module.exports = {
    dist: {
        options: {
            style: 'expanded'
        },
        files: {
            '<%= path.sass.temp %>': '<%= path.sass.src %>'
        }
    }
};