/* Grunt Template Variables */

module.exports = {

    sass: {
        src: 'src/scss/main.scss',
        all: ['src/scss/main.scss', 'src/scss/partials/*'],
        temp: 'scss/sass-compiled.css',
        concat: ['./node_modules/bootstrap/dist/css/bootstrap.min.css', 'scss/sass-compiled.css'],
        min: 'dist/main.min.css'
    },
    js: {
        src: 'src/js/**/*.js',
        temp: 'src/js/temp.js',
        dist: 'dist/main.min.js',
        spec: 'tests/spec/*.js'
    }

};