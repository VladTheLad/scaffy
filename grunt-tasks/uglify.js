/* Grunt UglifyJS Task Config */

module.exports = {
    my_target: {
        options: {
            compress: true
        },
        files: {
            '<%= path.js.dist %>': '<%= path.js.temp %>'
        }
    }
};