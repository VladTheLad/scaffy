/* Grunt Task Config for CSS Lint*/

module.exports = {
    strict: {
        options: {
            "import": 2,
            "adjoining-classes": false,
            "floats": false,
            "font-faces": false,
            "font-sizes": false,
            "ids": false,
            "duplicate-background-images": false,
            "order-alphabetical": false,
            "qualified-headings": false,
            "unique-headings": false
        },
        src: '<%= path.sass.temp %>'
    }
};