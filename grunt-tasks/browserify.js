/* Grunt Browserify Task Config */

module.exports = {
    dist: {
        files: {
            '<%= path.js.temp %>': ['<%= path.js.src %>']
        },
        options: {
        }
    }
};