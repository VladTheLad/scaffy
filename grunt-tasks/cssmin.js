/* Grunt CSS Minification Task Config */

module.exports = {
    options: {
        shorthandCompacting: false,
        roundingPrecision: -1
    },
    target: {
        files: {
            '<%= path.sass.min %>': '<%= path.sass.concat %>'
        }
    }
};