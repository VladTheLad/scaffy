/* Grunt JS Lint Task Config */

module.exports = {
    files: ['<%= path.js.src %>'],
    options: {
        curly: true,
        eqeqeq: true,
        freeze: true,
        forin: false,
        nonew: true,
        strict: true,
        undef: true,
        esversion: 5,
        maxdepth: 3,
        node: true,
        jquery: true,
        jasmine: true,
        globals: {
            console: true,
            web: true,
            window: true,
            document: true
        }
    }
};