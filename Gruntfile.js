module.exports = function (grunt) {

    grunt.initConfig({
        sass: require('./grunt-tasks/sass.js'),
        stylelint: require('./grunt-tasks/stylelint.js'),
        csslint: require('./grunt-tasks/csslint.js'),
        cssmin: require('./grunt-tasks/cssmin.js'),
        path:   require('./grunt-tasks/path.js'),
        jasmine: require('./grunt-tasks/jasmine.js'),
        jshint: require('./grunt-tasks/jshint.js'),
        browserify: require('./grunt-tasks/browserify.js'),
        uglify: require('./grunt-tasks/uglify.js'),
        clean: require('./grunt-tasks/clean.js'),
        watch: require('./grunt-tasks/watch.js')
    });

    /* AUTO-TASK-REGISTRATION FROM TASK NAME */
    require('jit-grunt')(grunt);

    /* CUSTOM TASK DECLARATION */
    grunt.registerTask('default', ['build']);
    grunt.registerTask('build', ['css', 'js']);
    grunt.registerTask('test', ['jasmine']);
    grunt.registerTask('css', ['sass', 'csslint', 'cssmin', 'clean']);
    grunt.registerTask('js', ['jshint', 'browserify', 'uglify', 'clean']);

};